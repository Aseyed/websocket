var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var fs = require('fs');
var forge = require('node-forge')
var rsa = forge.pki.rsa;
var pki = forge.pki;

var bodyParser = require('body-parser');
var content = fs.readFileSync('db.json');
//var content = require('db.json');
var db = JSON.parse(content);

app.use(express.static('views'));
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({extended: true}));
app.use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
    // Pass to next layer of middleware
    next();
});


app.use('/scripts',express.static(__dirname + '/node_modules'));
app.get('/', function (req, res, next) {
    res.render('index');
});

// redisClient.on('error', function (err) {
//     console.log(('Error' + err));
// });
var keypair = rsa.generateKeyPair({bits: 1024, e: 0x10001});

var rsa = forge.pki.rsa;
var pki = forge.pki;
var cipher = forge.rc2.createEncryptionCipher(key);

io.on('connection', function (client) {
    console.log('Client connected...');
    client.on('join', function (data) {
        console.log(data);
        client.emit('messages', 'Hello from server');
    });

    client.on('register', function (data) {
        console.log(data);
        var con = JSON.parse(data);
        var flag =0;
        for(var i in db.username){
            if(db.UserName[i] == con.Username)
                flag = 1;
            }

        if (flag){
            client.emit('register', {'ack': 'Ooops'});
        }
        else{
            con.PublicKey = pki.publicKeyFromPem(con.PublicKey);
            db.push(con);
            console.log(db)

            fs.writeFileSync("db.json", JSON.stringify(db));
            client.emit('register', {'ack': 'ok', 'PublicKey':keypair.publicKey});
        }

    });

    client.on('login', function (data) {
        con = JSON.parse(data);
        var message = forge.util.encode64(con.Signed);
        client.emit('login', message);
    })
})

var port = process.env.PORT || 2000
server.listen(port, function () {
    console.log('server created')
});